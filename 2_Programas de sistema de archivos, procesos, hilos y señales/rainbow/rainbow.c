/**
 * Programa que implementa tablas estilo rainbow con las sumas de verificación 
 * md5 y sha1 de la cadena que se pasa como parámetro o que se tiene en el archivo
 * que se especifica en la constante RAINBOW_FILE
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h> 
#include <unistd.h> 
#include <string.h>


#include <openssl/md5.h>
#include <openssl/sha.h>

#define MD5_DIGEST_LENGTH 16
#define SHA_DIGEST_LENGTH 20
#define CADENA_LENGTH 999999

#define RAINBOW_FILE "data_rainbow.bin"

int loop_sleep = 1;
int signal_sleep = 10;

char cadenaATrabajar[CADENA_LENGTH];
char* cadenaMD5;
char* cadenaSha1;

/**
 * Función que obtiene la longitud de la cadena de caracteres que almacenará la suma MD5
 * de las cadenas que operaremos.
 * @return El número de localidades en la cadena que obtendremos.
 */
int sizeOfMD5DigestLength() {
    return (MD5_DIGEST_LENGTH * 2 + 1);
}

/**
 * Función que obtiene la suma md5 de verificación de una cadena.
 * @param string Es la cadena de la que obtendremos la suma.
 * @param stringLength Es la longitud de la cadena pasada como parámetro.
 * @return La suma md5 de la cadena pasada como parámetro.
 */
char* getMD5(char* string, int stringLength) {
    unsigned char c[MD5_DIGEST_LENGTH];
    int i;
    char* result;
    MD5_CTX mdContext;


    MD5_Init(&mdContext);
    MD5_Update(&mdContext, string, stringLength);
    MD5_Final(c, &mdContext);
    result = malloc(sizeOfMD5DigestLength() * sizeof (char));
    for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&result[i * 2], "%02x", (unsigned int) c[i]);
    }
    return result;
}

/**
 * Función que obtiene la longitud de la cadena de caracteres que almacenará la suma sha1
 * de las cadenas que operaremos.
 * @return El número de localidades en la cadena que obtendremos.
 */
int sizeOfSha1DigestLength() {
    return (SHA_DIGEST_LENGTH * 2 + 1);
}

/**
 * Función que obtiene la suma sha1 de verificación de una cadena.
 * @param string Es la cadena de la que obtendremos la suma.
 * @param stringLength Es la longitud de la cadena pasada como parámetro.
 * @return La suma sha1 de la cadena pasada como parámetro.
 */
char* getSha1(char* string, int stringLength) {
    unsigned char digest[SHA_DIGEST_LENGTH];
    char* result;
    int i = 0;
    SHA_CTX ctx;
    SHA1_Init(&ctx);
    SHA1_Update(&ctx, string, stringLength);
    SHA1_Final(digest, &ctx);

    result = malloc(sizeOfMD5DigestLength() * sizeof (char));
    for (i = 0; i < SHA_DIGEST_LENGTH; i++) {
        sprintf(&result[i * 2], "%02x", (unsigned int) digest[i]);
    }
    return result;
}

/**
 * Función que almacena las tres variables que contienen la cadena que contiene
 * la entrada del usuario, la suma md5 y la suma sha1 de la cadena mencionada
 * anteriormente en un archivo binario mencionado en la constante RAINBOW_FILE
 */
void saveData() {
    FILE *my_file = fopen(RAINBOW_FILE, "wb");
    fwrite(&cadenaATrabajar, CADENA_LENGTH, sizeof (char), my_file);
    fwrite(cadenaMD5, sizeOfMD5DigestLength(), sizeof (char), my_file);
    fwrite(cadenaSha1, sizeOfSha1DigestLength(), sizeof (char), my_file);
    fclose(my_file);
}

/**
 * Función que, apartir de un código de señal, ejecuta ciertas tareas u otras.
 * @param signal Es la señal de decisión.
 */
void handle_signal(int signal) {

    switch (signal) {
        case SIGHUP:
            printf("Cadena: %s\nMD5: %s\nSha1: %s\n", cadenaATrabajar, cadenaMD5, cadenaSha1);
            break;
        case SIGINT:
            saveData();
            exit(0);
    }
    sleep(3);
}

int main() {
    struct sigaction sa;
    size_t size;
    char* linea;
    int tamanioTotal;

    linea = malloc(4000 * sizeof (char));
    FILE *my_file = fopen(RAINBOW_FILE, "rb");
    //Si el archivo existe entonces ignoramos la entrada de stdin.
    if (my_file) {
        printf("Se encontró archivo de configuración:\n");
        fread(&cadenaATrabajar, CADENA_LENGTH, sizeof (char), my_file);
        cadenaMD5 = malloc(sizeOfMD5DigestLength() * sizeof (char));
        fread(cadenaMD5, sizeOfMD5DigestLength(), sizeof (char), my_file);
        cadenaSha1 = malloc(sizeOfSha1DigestLength() * sizeof (char));
        fread(cadenaSha1, sizeOfSha1DigestLength(), sizeof (char), my_file);
        fclose(my_file);
    } else {
        printf("Ingrese una cadena a tratar:\nEscriba el caracter '.' para terminar de recibir entradas en la terminal:\n");
        while (getline(&linea, &size, stdin) != -1) {
            if (linea[0] == '.') {
                break;
            }
            strcat(cadenaATrabajar, linea);
        }
        free(linea);
        tamanioTotal = strlen(cadenaATrabajar);
        cadenaMD5 = getMD5(cadenaATrabajar, tamanioTotal);
        cadenaSha1 = getSha1(cadenaATrabajar, tamanioTotal);
    }

    printf("My pid is: %d\n", getpid());
    /* Setup the sighub handler */
    sa.sa_handler = &handle_signal;
    /* Restart the system call, if at all possible */
    sa.sa_flags = SA_RESTART;
    /* Block every signal during the handler */
    sigfillset(&sa.sa_mask);


    /* Intercept SIGHUP, SIGINT, SIGPIPE, SIGALRM, SIGTERM, SIGUSR1, SIGUSR2 and SIGCHLD */
    if (sigaction(SIGHUP, &sa, NULL) == -1)
        perror("Error: cannot handle SIGHUP"); /* Should not happen */

    if (sigaction(SIGINT, &sa, NULL) == -1)
        perror("Error: cannot handle SIGINT"); /* Should not happen */

    for (;;) {
        sleep(loop_sleep);
    }
}