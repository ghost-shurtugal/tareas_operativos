Hashdeep
----------------------------------------------------

## Pruebas

1. Se ocuparon los archivos del siguiente [direcotrio][1]
2. Los resultados de la prueba se encuentran en el siguiente [archivo][2]

## Consideraciones

1. Se obtienen los nombres de los archivos desde stdin y por medio de los argumentos del programa (cada localidad del arreglo es un archivo).
2. Se ignoran archivos no encontrados.



[1]:https://gitlab.com/ghost-shurtugal/tareas_operativos/tree/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/archivos_ejemplo
[2]:https://gitlab.com/ghost-shurtugal/tareas_operativos/blob/master/2_Programas%20de%20sistema%20de%20archivos,%20procesos,%20hilos%20y%20se%C3%B1ales/salidas/salida_hashdeep.txt



