Bus info          Device      Class          Description
========================================================
                              system         HP ProDesk 600 G1 SFF (C8T89AV#117)
                              bus            18E7
                              memory         64KiB BIOS
cpu@0                         processor      Intel(R) Core(TM) i5-4570 CPU @ 3.20GHz
                              memory         1MiB L2 cache
                              memory         256KiB L1 cache
                              memory         6MiB L3 cache
                              memory         4GiB System Memory
                              memory         DIMM [empty]
                              memory         DIMM [empty]
                              memory         DIMM [empty]
                              memory         4GiB DIMM DDR3 Synchronous 1600 MHz (0.6 ns)
pci@0000:00:00.0              bridge         4th Gen Core Processor DRAM Controller
pci@0000:00:02.0              display        Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller
pci@0000:00:03.0              multimedia     Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller
pci@0000:00:14.0              bus            8 Series/C220 Series Chipset Family USB xHCI
pci@0000:00:16.0              communication  8 Series/C220 Series Chipset Family MEI Controller #1
pci@0000:00:16.3              communication  8 Series/C220 Series Chipset Family KT Controller
pci@0000:00:19.0  eno1        network        Ethernet Connection I217-LM
pci@0000:00:1a.0              bus            8 Series/C220 Series Chipset Family USB EHCI #2
pci@0000:00:1b.0              multimedia     8 Series/C220 Series Chipset High Definition Audio Controller
pci@0000:00:1d.0              bus            8 Series/C220 Series Chipset Family USB EHCI #1
pci@0000:00:1f.0              bridge         Q85 Express LPC Controller
pci@0000:00:1f.2              storage        8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]
pci@0000:00:1f.3              bus            8 Series/C220 Series Chipset Family SMBus Controller
                  scsi0       storage        
scsi@0:0.0.0      /dev/sda    disk           500GB WDC WD5000AAKX-6
scsi@0:0.0.0,1    /dev/sda1   volume         498MiB Windows NTFS volume
scsi@0:0.0.0,2    /dev/sda2   volume         99MiB Windows FAT volume
scsi@0:0.0.0,3    /dev/sda3   volume         15MiB reserved partition
scsi@0:0.0.0,4    /dev/sda4   volume         465GiB Windows NTFS volume
                  scsi2       storage        
scsi@2:0.0.0      /dev/cdrom  disk           CDDVDW SN-208FB
usb@3:7           scsi6       storage        
scsi@6:0.0.0      /dev/sdb    disk           8178MB UDisk
                  /dev/sdb    disk           8178MB 
                  /dev/sdb2   volume         15EiB Windows FAT volume
usb@3:9           scsi7       storage        
scsi@7:0.0.0      /dev/sdc    disk           USB3.0 CRW-CF/MD
                  /dev/sdc    disk           
scsi@7:0.0.1      /dev/sdd    disk           USB3.0 CRW-SD
                  /dev/sdd    disk           
scsi@7:0.0.2      /dev/sde    disk           USB3.0 CRW-MS/HG
                  /dev/sde    disk           
                              power          Standard Efficiency
